# IVySoft.GitLabTools
Command line tools to automate GitLab tasks

## Installation

```
dotnet tool install IVySoft.Gitlab.Tools --global
```

## Usage

### Run manual job

```
gitlabtools run-manual-job -s ServerUrl -t PrivateToken -p Porject -j JobName -b  BranchName
```

### Build nuget packages dependencies

```
gitlabtools nuget-tree -s ServerUrl -u UserName -t PrivateToken -g GroupId -p PackageName -v PackageVersion [-c Check token access Project Id or Namespace]
```
-c: Specify target project id or namespace to target project

### Check job-token-scope settings
```
gitlabtools check-job-token-scope -s ServerUrl -u UserName -t PrivateToken -g GroupId -c TargetProjectIdOrNamespace
```
-c: Specify target project id or namespace to target project
