﻿using IVySoft.Gitlab.Api;
using IVySoft.Gitlab.Tools.Options;

namespace IVySoft.Gitlab.Tools
{
    internal class RunNugetTreeJob : NugetTreeJobBase
    {
        private NugetTreeOptions opts;

        public RunNugetTreeJob(NugetTreeOptions opts)
        {
            this.opts = opts;
        }
        internal async Task<int> Run(string gitlabToken, CancellationToken cancellationToken)
        {
            using var client = new GitLabClient(opts.ServerUrl, opts.User, gitlabToken);

            var unproccessed = new Queue<TargetPackage>();
            var sources = new Dictionary<string/*package/version*/, List<string>>();
            if (!string.IsNullOrWhiteSpace(opts.Package))
            {
                unproccessed.Enqueue(new(opts.Package, opts.Version));
            }
            else
            {
                CollectPackages(Environment.CurrentDirectory, unproccessed, sources);
            }
            var projects = await CollectPackagesTree(client, opts.GroupId, unproccessed, cancellationToken);
            foreach (var project in projects)
            {
                var projectInfo = await client.Projects.SingleOrDefault(new Api.Models.Projects.Requests.ProjectSingleQuery(project.Key.ToString()), cancellationToken);
                if (null != projectInfo)
                {
                    Console.WriteLine($"Project {project.Key}({projectInfo.WebUrl}):");
                }
                else
                {
                    Console.WriteLine($"Project {project.Key}:");
                }

                foreach (var package in project.Value)
                {
                    if (1 < package.Value.Count)
                    {
                        Console.WriteLine($"  Warning: {package.Key} have multiple versions");
                    }
                    foreach (var packageVersion in package.Value)
                    {
                        Console.WriteLine($"  {package.Key} {packageVersion.Key}:");
                        if (sources.TryGetValue($"{package.Key} {packageVersion.Key}", out var packageSources))
                        {
                            foreach (var source in packageSources)
                            {
                                Console.WriteLine($"  Source: {source}");
                            }
                        }
                        foreach (var dependencyGroup in packageVersion.Value)
                        {
                            Console.WriteLine($"    {dependencyGroup.TargetFramework}:");
                            foreach (var dependency in dependencyGroup.Dependencies)
                            {
                                Console.WriteLine($"      {dependency.PackageName} {dependency.PackageRange}");
                            }
                        }
                    }
                }
            }
            return 0;
        }
    }
}