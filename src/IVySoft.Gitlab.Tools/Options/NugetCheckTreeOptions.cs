﻿using CommandLine;

namespace IVySoft.Gitlab.Tools.Options
{
    [Verb("nuget-check-tree")]
    internal class NugetCheckTreeOptions : JobBaseOptions
    {
        [Option('g', "group", Required = true, HelpText = "Group id")]
        public int GroupId { get; set; }
    }
}
