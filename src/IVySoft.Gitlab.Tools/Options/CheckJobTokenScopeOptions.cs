﻿using CommandLine;

namespace IVySoft.Gitlab.Tools.Options
{
    [Verb("check-job-token-scope")]
    internal class CheckJobTokenScopeOptions : JobBaseOptions
    {
        [Option('g', "group", Required = true, HelpText = "Group id")]
        public int GroupId { get; set; }
        [Option('c', "check-access", Required = false, HelpText = "Check job token access settings to project")]
        public string CheckAccess { get; set; } = string.Empty;
    }
}
