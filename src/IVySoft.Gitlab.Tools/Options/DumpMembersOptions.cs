﻿using CommandLine;

namespace IVySoft.Gitlab.Tools.Options
{
    [Verb("dump-members")]
    internal class DumpMembersOptions : JobBaseOptions
    {
        [Option('g', "group", Required = true, HelpText = "Group id")]
        public long GroupId { get; set; }
        [Option('a', "all-members", Required = false, HelpText = "List all members of a group or project including inherited and invited members")]
        public bool AllMembers { get; set; }
        [Option("no-projects", Required = false, HelpText = "Skip sub projects")]
        public bool NoSubProjects { get; set; }
        [Option("no-groups", Required = false, HelpText = "Skip sub groups")]
        public bool NoSubGroups { get; set; }
        [Option("include-tokens", Required = false, HelpText = "Include tokens")]
        public bool IncludeTokens { get; set; }
    }
}
