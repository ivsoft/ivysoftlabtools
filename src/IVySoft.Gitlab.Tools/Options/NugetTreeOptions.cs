﻿using CommandLine;

namespace IVySoft.Gitlab.Tools.Options
{
    [Verb("nuget-tree")]
    internal class NugetTreeOptions : JobBaseOptions
    {
        [Option('g', "group", Required = true, HelpText = "Group id")]
        public int GroupId { get; set; }
        [Option('p', "package", Required = false, HelpText = "Package id")]
        public string Package { get; set; } = null!;
        [Option('v', "version", Required = false, HelpText = "Package version")]
        public string Version { get; set; } = null!;
    }
}
