﻿using CommandLine;
using IVySoft.Gitlab.Tools.Options;
namespace IVySoft.Gitlab.Tools.Options
{
    internal class JobBaseOptions
    {
        [Option('s', "server", Required = true, HelpText = "Gitlab server Url")]
        public string ServerUrl { get; set; } = null!;
        [Option('u', "user", Required = true, HelpText = "Gitlab user name")]
        public string User { get; set; } = null!;
        [Option('t', "token", Required = false, HelpText = "Gitlab server token")]
        public string? Token { get; set; }
    }
}