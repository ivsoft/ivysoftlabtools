﻿using CommandLine;

namespace IVySoft.Gitlab.Tools.Options
{
    [Verb("run-manual-job")]
    internal class RunManualJobOptions : JobBaseOptions
    {
        [Option('p', "project", Required = true, HelpText = "Project id")]
        public string Project { get; set; } = null!;
        [Option('b', "branch", Required = false, HelpText = "Branch id")]
        public string Branch { get; set; } = "main";
        [Option("tag", Required = false, HelpText = "Branch id")]
        public string Tag { get; set; } = string.Empty;
        [Option('j', "job-name", Required = true, HelpText = "Job name to execute")]
        public string JobName { get; set; } = null!;
        [Option("no-wait", Required = false, HelpText = "Don't wait for pipeline")]
        public bool NoWait { get; set; }
        [Option("force", Required = false, HelpText = "Ignore failed jobs")]
        public bool Force { get; set; }
    }

}
