﻿using IVySoft.Gitlab.Api;
using IVySoft.Gitlab.Api.Models.Packages;
using IVySoft.Gitlab.Tools.Options;

namespace IVySoft.Gitlab.Tools
{
    internal class RunNugetCheckTreeJob : NugetTreeJobBase
    {
        private NugetCheckTreeOptions opts;

        public RunNugetCheckTreeJob(NugetCheckTreeOptions opts)
        {
            this.opts = opts;
        }
        internal async Task<int> Run(string gitlabToken, CancellationToken cancellationToken)
        {
            using var client = new GitLabClient(opts.ServerUrl, opts.User, gitlabToken);

            var unproccessed = new Queue<TargetPackage>();
            var sources = new Dictionary<string/*package/version*/, List<string>>();
            CollectPackages(Environment.CurrentDirectory, unproccessed, sources);
            var projects = await CollectPackagesTree(client, opts.GroupId, unproccessed, cancellationToken);
            var parents = new Dictionary<string, HashSet<string>>();
            foreach (var project in projects)
            {
                foreach (var package in project.Value)
                {
                    foreach (var packageVersion in package.Value)
                    {
                        var parent = $"{package.Key} {packageVersion.Key}";
                        foreach (var dependencyGroup in packageVersion.Value)
                        {
                            foreach (var dependency in dependencyGroup.Dependencies)
                            {
                                var child = $"{dependency.PackageName} {dependency.PackageRange}";
                                if (parents.TryGetValue(child, out var parentItems))
                                {
                                    parentItems.Add(parent);
                                }
                                else
                                {
                                    parentItems = new HashSet<string> { parent };
                                    parents.Add(child, parentItems);
                                }
                            }
                        }
                    }
                }
            }

            int result = 0;
            foreach (var project in projects)
            {
                foreach (var package in project.Value)
                {
                    if (1 < package.Value.Count)
                    {
                        foreach (var packageVersion in package.Value)
                        {
                            var packageId = $"{package.Key} {packageVersion.Key}";
                            Console.Write(packageId);
                            foreach(var item in GetParentPath(parents, packageId, sources))
                            {
                                Console.Write("<-");
                                Console.Write(item);
                            }
                            Console.WriteLine();
                            result = 1;
                        }
                    }
                }
            }
            if(0 == result)
            {
                Console.WriteLine("No duplicates");
            }
            return result;
        }

        private static string[] GetParents(Dictionary<string, HashSet<string>> parents, string target, Dictionary<string/*package version*/, List<string>> sources)
        {
            if(parents.TryGetValue(target, out var result))
            {
                return result.ToArray();
            }
            else if(sources.TryGetValue(target, out var source))
            {
                return new string[] { string.Join(',', source) };
            }
            else
            {
                return Array.Empty<string>();
            }
        }
        private static string[] GetParentPath(Dictionary<string, HashSet<string>> parents, string target, Dictionary<string/*package/version*/, List<string>> sources)
        {
            var result = Array.Empty<string>();
            foreach (var parent in GetParents(parents, target, sources))
            {
                var child = GetParentPath(parents, parent, sources);
                if(child.Length + 1 > result.Length)
                {
                    result = new string[] { parent }.Concat(child).ToArray();
                }
            }
            return result;
        }
    }
}