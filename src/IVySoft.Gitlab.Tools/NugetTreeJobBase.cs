﻿using IVySoft.Gitlab.Api;
using IVySoft.Gitlab.Api.Models.Packages;
using IVySoft.Gitlab.Api.Models.Packages.Requests;
using System.Diagnostics;
using System.Text.Json.Nodes;
namespace IVySoft.Gitlab.Tools
{
    internal class NugetTreeJobBase
    {
        protected static async Task<Dictionary<long /*projectId*/, Dictionary<string/*packageName*/, Dictionary<string /*packageVersion*/, List<IPackageDependencyGroup>>>>> CollectPackagesTree(
            GitLabClient client,
            int groupId,
            Queue<TargetPackage> unproccessed,
            CancellationToken cancellationToken)
        {
            var reportTime = DateTime.Now.AddSeconds(5);
            var processed = new HashSet<string>();
            var projects = new Dictionary<long, Dictionary<string, Dictionary<string, List<IPackageDependencyGroup>>>>();
            while (0 < unproccessed.Count)
            {
                var now = DateTime.Now;
                if(now > reportTime)
                {
                    Console.WriteLine($"{processed.Count}/{(processed.Count + unproccessed.Count)}");
                    reportTime = now.AddSeconds(5);
                }
                var (packageName, packageVersion) = unproccessed.Dequeue();
                processed.Add($"{packageName}/{packageVersion}");
                await foreach (var package in client.Packages.Query(new PackagesQuery(groupId)
                {
                    PackageName = packageName,
                    PackageVersion = packageVersion,
                    PackageType = "nuget"
                }, cancellationToken))
                {
                    if (package.Name == packageName && package.Version == packageVersion)
                    {
                        if (!projects.TryGetValue(package.ProjectId, out var projectInfo))
                        {
                            projectInfo = new();
                            projects.Add(package.ProjectId, projectInfo);
                        }
                        if (!projectInfo.TryGetValue(package.Name, out var packages))
                        {
                            packages = new();
                            projectInfo.Add(package.Name, packages);
                        }
                        if (!packages.TryGetValue(package.Version, out var dependencies))
                        {
                            dependencies = new();
                            await foreach (var dependencyGroup in client.Packages.GetDependencies(
                                package.ProjectId.ToString(),
                                package.Name,
                                package.Version,
                                cancellationToken))
                            {
                                dependencies.Add(dependencyGroup);
                                foreach (var dependency in dependencyGroup.Dependencies)
                                {
                                    if (!processed.Contains($"{dependency.PackageName}/{dependency.PackageRange}")
                                        && !unproccessed.Any(x => x.PackageName == dependency.PackageName && x.PackageVersion == dependency.PackageRange))
                                    {
                                        unproccessed.Enqueue(new TargetPackage(dependency.PackageName, dependency.PackageRange));
                                    }
                                }
                            }
                            packages.Add(package.Version, dependencies);
                        }
                        break;
                    }
                }
            }

            return projects;
        }

        protected static void CollectPackages(
            string currentDirectory,
            Queue<TargetPackage> unproccessed,
            Dictionary<string/*package/version*/, List<string>>? sources)
        {
            foreach(var directory in Directory.GetDirectories(currentDirectory))
            {
                CollectPackages(directory, unproccessed, sources);
            }
            if ("obj" == Path.GetFileName(currentDirectory))
            {
                foreach (var file in Directory.GetFiles(currentDirectory, "project.assets.json"))
                {
                    try
                    {
                        using var stream = File.OpenRead(file);
                        var info = JsonNode.Parse(stream);
                        if (null == info)
                        {
                            throw new Exception("Unable to parse file");
                        }
                        var targets = info["targets"];
                        if(null == targets)
                        {
                            throw new Exception("No targets");
                        }
                        foreach(KeyValuePair<string, JsonNode?> framework in targets.AsObject())
                        {
                            if(null != framework.Value)
                            {
                                foreach (KeyValuePair<string, JsonNode?> dependency in framework.Value.AsObject())
                                {
                                    if (null != dependency.Value && "package" == (string?)dependency.Value["type"])
                                    {
                                        var index = dependency.Key.LastIndexOf('/');
                                        if (index < 0)
                                        {
                                            throw new Exception($"Invalid dependency {dependency.Key}");
                                        }
                                        var package = dependency.Key.Substring(0, index);
                                        var version = dependency.Key.Substring(index + 1);
                                        if (!unproccessed.Any(x => x.PackageName == package && x.PackageVersion == version))
                                        {
                                            unproccessed.Enqueue(new TargetPackage(package, version));
                                        }
                                        if (null != sources)
                                        {
                                            var key = $"{package} {version}";
                                            if (!sources.TryGetValue(key, out var exists))
                                            {
                                                sources.Add(key, new List<string> { file });
                                            }
                                            else
                                            {
                                                exists.Add(file);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine($"Warning: {ex.Message} at parsing file {file}");
                    }
                }
            }
        }
    }
    internal readonly record struct TargetPackage(string PackageName, string PackageVersion);
}