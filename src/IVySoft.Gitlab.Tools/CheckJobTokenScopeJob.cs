﻿using IVySoft.Gitlab.Api;
using IVySoft.Gitlab.Tools.Options;
using System.Text.Json.Nodes;

namespace IVySoft.Gitlab.Tools
{
    internal class CheckJobTokenScopeJob : NugetTreeJobBase
    {
        private CheckJobTokenScopeOptions opts;

        public CheckJobTokenScopeJob(CheckJobTokenScopeOptions opts)
        {
            this.opts = opts;
        }
        internal async Task<int> Run(string gitlabToken, CancellationToken cancellationToken)
        {
            if(string.IsNullOrWhiteSpace(opts.CheckAccess))
            {
                opts.CheckAccess = DetectNamespace();
            }
            Console.WriteLine($"Checking job token access for project {opts.CheckAccess}");
            using var client = new GitLabClient(opts.ServerUrl, opts.User, gitlabToken);

            var unproccessed = new Queue<TargetPackage>();
            CollectPackages(Environment.CurrentDirectory, unproccessed, null);
            var projects = await CollectPackagesTree(client, opts.GroupId, unproccessed, cancellationToken);
            foreach (var project in projects)
            {
                var projectInfo = await client.Projects.SingleOrDefault(new Api.Models.Projects.Requests.ProjectSingleQuery(project.Key.ToString()), cancellationToken);
                if (null != projectInfo)
                {
                    Console.WriteLine($"Project {project.Key}({projectInfo.WebUrl}):");
                }
                else
                {
                    Console.WriteLine($"Project {project.Key}:");
                }

                var jobTokenScope = await client.Projects.GetJobTokenScope(project.Key.ToString(), cancellationToken);
                if (!jobTokenScope.InboundEnabled)
                {
                    Console.WriteLine("inbound check disabled");
                }
                else
                {
                    Console.Write("inbound check enabled, ");

                    bool isAllowed = false;
                    await foreach (var allowList in client.Projects.GetJobTokenScopeAllowList(project.Key.ToString(), cancellationToken))
                    {
                        if (allowList.PathWithNamespace == opts.CheckAccess)
                        {
                            isAllowed = true;
                            break;
                        }
                    }
                    Console.WriteLine(
                        isAllowed
                        ? $"{opts.CheckAccess} in the while list"
                        : $"{opts.CheckAccess} not in the while list");
                }
            }
            return 0;
        }

        private string DetectNamespace()
        {
            for(var current = Environment.CurrentDirectory; current != null;)
            {
                var git = Path.Combine(current, ".git");
                if(Directory.Exists(git))
                {
                    using var stream = File.OpenText(Path.Combine(git, "config"));
                    bool inOrigin = false;
                    for(; ; )
                    {
                        var line = stream.ReadLine();
                        if(null == line)
                        {
                            break;
                        }
                        line = line.Trim();
                        if("[remote \"origin\"]" == line)
                        {
                            inOrigin = true;
                        }
                        else if(inOrigin)
                        {
                            if(line.StartsWith('['))
                            {
                                inOrigin = false;
                            }
                            else
                            {
                                var index = line.IndexOf('=');
                                if(index >= 0)
                                {
                                    var key = line.Substring(0, index).Trim();
                                    if ("url" == key)
                                    {
                                        var value = line.Substring(index + 1).Trim();
                                        if(value.EndsWith(".git"))
                                        {
                                            value = value.Substring(0, value.Length - ".git".Length);
                                        }
                                        try
                                        {
                                            return new Uri(value).AbsolutePath.Trim('/');
                                        }
                                        catch(UriFormatException )
                                        {
                                            return new Uri("http://" + value.Replace(':', '/')).AbsolutePath.Trim('/');
                                        }
                                    }
                                }
                            }
                        }
                    }
                    throw new Exception($"Unable to read git url from config {Path.Combine(git, "config")}");
                }
                var next = Path.GetDirectoryName(current);
                if(string.IsNullOrEmpty(next) || next == current)
                {
                    break;
                }
                current = next;
            }
            throw new Exception("not a git repository (or any of the parent directories): .git");
        }
    }
}