﻿using CommandLine;
using IVySoft.Gitlab.Api;
using IVySoft.Gitlab.Api.Models.Jobs.Requests;
using IVySoft.Gitlab.Api.Models.MergeRequests.Requests;
using IVySoft.Gitlab.Api.Models.Pipelines;
using IVySoft.Gitlab.Api.Models.Pipelines.Requests;
using IVySoft.Gitlab.Api.Models.Projects.Requests;
using IVySoft.Gitlab.Tools.Options;
using System.Data;

namespace IVySoft.Gitlab.Tools;

public class Program
{
    public static async Task<int> Main(string[] args)
    {
        var cancellationSource = new CancellationTokenSource();
        Console.CancelKeyPress += (_, _) => cancellationSource.Cancel();

        return await Parser.Default.ParseArguments<
            RunManualJobOptions,
            NugetTreeOptions,
            NugetCheckTreeOptions,
            CheckJobTokenScopeOptions,
            DumpMembersOptions>(args)
        .MapResult(
            (RunManualJobOptions opts) => new RunManualJob(opts).Run(GetToken(opts), cancellationSource.Token),
            (NugetTreeOptions opts) => new RunNugetTreeJob(opts).Run(GetToken(opts), cancellationSource.Token),
            (NugetCheckTreeOptions opts) => new RunNugetCheckTreeJob(opts).Run(GetToken(opts), cancellationSource.Token),
            (CheckJobTokenScopeOptions opts) => new CheckJobTokenScopeJob(opts).Run(GetToken(opts), cancellationSource.Token),
            (DumpMembersOptions opts) => new DumpMembersJob(opts).Run(GetToken(opts), cancellationSource.Token),
            errs => Task.FromResult(-1)); // Invalid arguments
    }

    private static string GetToken(JobBaseOptions opts)
    {
        if(null != opts.Token)
        {
            return opts.Token;
        }
        var result = Environment.GetEnvironmentVariable("GITLAB_TOKEN");
        if (null != result)
        {
            return result;
        }
        throw new Exception("Please specify the gitlab token as the --token command line parameter or in the GITLAB_TOKEN environment variable");
    }
}
