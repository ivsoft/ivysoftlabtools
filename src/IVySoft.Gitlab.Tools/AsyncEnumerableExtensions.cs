﻿namespace IVySoft.Gitlab.Tools;

internal static class AsyncEnumerableExtensions
{
    public static async ValueTask<T?> FirstOrDefault<T>(this IAsyncEnumerable<T> enumerable)
    {
        await foreach(var item in enumerable)
        {
            return item;
        }
        return default(T);
    }
}