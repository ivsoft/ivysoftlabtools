﻿using IVySoft.Gitlab.Api;
using IVySoft.Gitlab.Api.Models.Jobs.Requests;
using IVySoft.Gitlab.Api.Models.MergeRequests.Requests;
using IVySoft.Gitlab.Api.Models.Pipelines;
using IVySoft.Gitlab.Api.Models.Pipelines.Requests;
using IVySoft.Gitlab.Api.Models.Projects.Requests;
using IVySoft.Gitlab.Tools.Options;

namespace IVySoft.Gitlab.Tools
{
    internal class RunManualJob
    {
        private RunManualJobOptions opts;

        public RunManualJob(RunManualJobOptions opts)
        {
            this.opts = opts;
        }

        internal async Task<int> Run(string gitlabToken, CancellationToken cancellationToken)
        {
            using var client = new GitLabClient(opts.ServerUrl, opts.User, gitlabToken);
            var project = await client.Projects.SingleOrDefault(new ProjectSingleQuery(opts.Project), cancellationToken);
            if (null == project)
            {
                throw new Exception($"Project {opts.Project} not found on server {opts.ServerUrl}");
            }
            var pipelineId = await LoadPipeline(client, opts.JobName, cancellationToken);
            if (null == pipelineId)
            {
                throw new Exception($"Pipeline in branch {opts.Branch} not found in the project {project.WebUrl}");
            }
            if (!opts.NoWait)
            {
                for (bool bContinue = true; bContinue;)
                {
                    bContinue = false;
                    var pipeline = await client.Pipelines.SingleOrDefault(
                        new PipelineSingleQuery(opts.Project, pipelineId.Value),
                        cancellationToken);
                    if (null == pipeline)
                    {
                        throw new Exception($"Pipeline {pipelineId.Value} in branch {opts.Branch} not found in the project {project.WebUrl}");
                    }
                    Console.WriteLine($"{pipeline.WebUrl} {pipeline.Status}");
                    switch (pipeline.Status)
                    {
                        case "waiting_for_resource":
                        case "preparing":
                        case "pending":
                        case "running":
                        case "scheduled":
                            bContinue = true;
                            break;
                    }
                    await foreach (var job in client.Jobs.Query(
                        new JobsQuery(opts.Project, pipeline.Id),
                        cancellationToken))
                    {
                        Console.WriteLine($"{job.WebUrl} {job.Status} {job.Name}");
                        switch (job.Status)
                        {
                            case "waiting_for_resource":
                            case "preparing":
                            case "pending":
                            case "running":
                            case "scheduled":
                                bContinue = true;
                                break;
                        }
                    }
                    if (bContinue)
                    {
                        await Task.Delay(5000);
                    }
                }
            }
            await foreach (var job in client.Jobs.Query(
                new JobsQuery(opts.Project, pipelineId.Value),
                cancellationToken))
            {
                if (opts.JobName == job.Name && job.Status != "success")
                {
                    switch (job.Status)
                    {
                        case "failed":
                            if (!opts.Force)
                            {
                                return 1;
                            }
                            break;
                    }
                    Console.WriteLine($"{job.WebUrl} {job.Status} {job.Name}");
                    await client.Jobs.Run(project.Id, job.Id, cancellationToken);
                }
            }
            if (!opts.NoWait)
            {
                for (bool bContinue = true; bContinue;)
                {
                    bContinue = false;
                    var pipeline = await client.Pipelines.SingleOrDefault(
                        new PipelineSingleQuery(opts.Project, pipelineId.Value),
                        cancellationToken);
                    if (null == pipeline)
                    {
                        throw new Exception($"Pipeline {pipelineId.Value} in branch {opts.Branch} not found in the project {project.WebUrl}");
                    }
                    Console.WriteLine($"{pipeline.WebUrl} {pipeline.Status}");
                    switch (pipeline.Status)
                    {
                        case "waiting_for_resource":
                        case "preparing":
                        case "pending":
                        case "running":
                        case "scheduled":
                            bContinue = true;
                            break;
                        case "failed":
                        case "canceled":
                        case "skipped":
                            if (!opts.Force)
                            {
                                return 1;
                            }
                            break;
                    }
                    await foreach (var job in client.Jobs.Query(
                        new JobsQuery(opts.Project, pipeline.Id),
                        cancellationToken))
                    {
                        Console.WriteLine($"{job.WebUrl} {job.Status} {job.Name}");
                        switch (job.Status)
                        {
                            case "waiting_for_resource":
                            case "preparing":
                            case "pending":
                            case "running":
                            case "scheduled":
                                bContinue = true;
                                break;
                            case "failed":
                            case "canceled":
                                if (opts.JobName == job.Name || !opts.Force)
                                {
                                    return 1;
                                }
                                break;
                        }
                    }

                    if (bContinue)
                    {
                        await Task.Delay(5000);
                    }
                }
            }
            return 0;
        }

        private async ValueTask<long?> LoadPipeline(
            GitLabClient client,
            string jobName,
            CancellationToken cancellationToken)
        {
            string? sha = null;
            if(!string.IsNullOrWhiteSpace(opts.Tag))
            {
                var tag = await client.Tags.SingleOrDefault(opts.Project, opts.Tag, cancellationToken);
                if (null == tag)
                {
                    throw new Exception($"Tag '{opts.Tag}' not found in the project '{opts.Project}'");
                }
                sha = tag.Commit.ParentIds.SingleOrDefault();
            }
            await foreach (var mergeRequest in client.MergeRequests.Query(
                new MergeRequestsQuery(opts.Project)
                {
                    SourceBranch = opts.Branch,
                    State = "opened"
                },
                cancellationToken))
            {
                var mr_pipeline = await client.Pipelines.Query(
                    new PipelinesQuery(opts.Project)
                    {
                        Order = PipelineOrder.Id,
                        SortOrder = Api.Models.SortOrder.Descending,
                        Ref = $"refs/merge-requests/{mergeRequest.IId}/head",
                        Sha = sha,
                        PerPage = 1
                    },
                    cancellationToken).FirstOrDefault();
                if (null != mr_pipeline)
                {
                    await foreach (var job in client.Jobs.Query(
                        new JobsQuery(opts.Project, mr_pipeline.Id),
                        cancellationToken))
                    {
                        if (job.Name == jobName)
                        {
                            return mr_pipeline.Id;
                        }
                    }
                }
            }
            var directPipeline = await client.Pipelines.Query(
                new PipelinesQuery(opts.Project)
                {
                    Order = PipelineOrder.Id,
                    SortOrder = Api.Models.SortOrder.Descending,
                    Ref = opts.Branch,
                    Sha = sha,
                    PerPage = 1
                },
                cancellationToken).FirstOrDefault();
            if (null != directPipeline)
            {
                return directPipeline.Id;
            }
            return null;
        }
    }
}