﻿using IVySoft.Gitlab.Api;
using IVySoft.Gitlab.Api.Models.Groups;
using IVySoft.Gitlab.Api.Models.Members;
using IVySoft.Gitlab.Api.Models.Projects;
using IVySoft.Gitlab.Tools.Options;
using System.IO;

namespace IVySoft.Gitlab.Tools
{
    internal class DumpMembersJob : NugetTreeJobBase
    {
        private DumpMembersOptions opts;

        public DumpMembersJob(DumpMembersOptions opts)
        {
            this.opts = opts;
        }

        internal async Task<int> Run(string gitlabToken, CancellationToken cancellationToken)
        {
            using var client = new GitLabClient(opts.ServerUrl, opts.User, gitlabToken);
            var group = await client.Groups.Single(opts.GroupId, cancellationToken);
            Console.WriteLine($"{group.Name}: Group {group.Id} {group.WebUrl}:");
            foreach (var member in group.SharedWithGroups)
            {
                Console.WriteLine($"  {member.GroupName} (Group) - {(MemberAccessLevel)member.AccessLevel}");
            }
            await DumpGroupMembers(client, opts.GroupId, $"{group.Name}/", cancellationToken);
            return 0;
        }
        private async ValueTask DumpGroupMembers(IGitLabClient client, long groupId, string path, CancellationToken cancellationToken)
        {
            await foreach (var member in opts.AllMembers
                ? client.Groups.GetAllMembers(new GroupMembersQuery(groupId), cancellationToken)
                : client.Groups.GetMembers(new GroupMembersQuery(groupId), cancellationToken))
            {
                Console.WriteLine($"  {member.Name} {member.Email} - {(MemberAccessLevel)member.AccessLevel}");
            }

            var groups = new List<IGroup>();
            if (!opts.NoSubGroups)
            {
                await foreach (var group in client.Groups.SubGroups(groupId, cancellationToken))
                {
                    groups.Add(group);
                }
            }
            var projects = new List<IProject>();
            if (!opts.NoSubProjects)
            {
                await foreach (var project in client.Groups.SubProjects(groupId, cancellationToken))
                {
                    projects.Add(project);
                }
            }
            foreach (var project in projects)
            {
                Console.WriteLine($"{path}{project.Name}: Project {project.Id} {project.WebUrl}:");
                foreach (var member in project.SharedWithGroups)
                {
                    Console.WriteLine($"  {member.GroupName} (Group) - {(MemberAccessLevel)member.AccessLevel}");
                }
                await foreach (var member in 
                    opts.AllMembers
                    ? client.Projects.GetAllMembers(new ProjectMembersQuery(project.Id), cancellationToken)
                    : client.Projects.GetMembers(new ProjectMembersQuery(project.Id), cancellationToken))
                {
                    Console.WriteLine($"  {member.Name} {member.Email} - {(MemberAccessLevel)member.AccessLevel}");
                }
            }
            foreach (var group in groups)
            {
                Console.WriteLine($"{path}{group.Name}: Group {group.Id} {group.WebUrl}:");
                foreach (var member in group.SharedWithGroups)
                {
                    Console.WriteLine($"  {member.GroupName} (Group) - {(MemberAccessLevel)member.AccessLevel}");
                }
                await DumpGroupMembers(client, group.Id, $"{path}{group.Name}/", cancellationToken);
            }
        }
    }
}