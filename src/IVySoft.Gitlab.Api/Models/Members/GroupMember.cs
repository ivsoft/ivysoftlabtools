﻿using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.Members
{
    internal class GroupMember : IGroupMember
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; } = null!;
    }
}
