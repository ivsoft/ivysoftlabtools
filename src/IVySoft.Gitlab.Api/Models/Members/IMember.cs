﻿namespace IVySoft.Gitlab.Api.Models.Members
{
    public interface IMember
    {
        public long Id { get; }
        public string Name { get; }
        public string Email { get; }
        public DateTime? ExpiresAt { get; }
        public int AccessLevel { get; }
    }
}
