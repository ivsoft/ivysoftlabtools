﻿namespace IVySoft.Gitlab.Api.Models.Members
{
    public enum MemberAccessLevel
    {
        NoAccess = 0,
        Minimal = 5,
        Guest = 10,
        Reporter = 20,
        Developer = 30,
        Maintainer = 40,
        Owner = 50
    }
}
