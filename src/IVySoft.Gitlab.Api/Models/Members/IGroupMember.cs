﻿namespace IVySoft.Gitlab.Api.Models.Members
{
    public interface IGroupMember
    {
        public long Id { get; }
        public string Name { get; }
    }
}
