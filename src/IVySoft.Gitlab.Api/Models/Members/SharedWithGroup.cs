﻿using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.Members
{
    public class SharedWithGroup : ISharedWithGroup
    {
        [JsonPropertyName("group_id")]
        public long GroupId { get; set; }

        [JsonPropertyName("group_name")]
        public string GroupName { get; set; } = null!;

        [JsonPropertyName("group_full_path")]
        public string GroupFullPath { get; set; } = null!;

        [JsonPropertyName("group_access_level")]
        public MemberAccessLevel AccessLevel { get; set; }
    }
}