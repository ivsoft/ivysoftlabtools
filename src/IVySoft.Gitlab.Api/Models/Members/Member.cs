﻿using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.Members
{
    internal class Member : IMember
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; } = null!;
        [JsonPropertyName("email")]
        public string Email { get; set; } = null!;
        [JsonPropertyName("expires_at")]
        public DateTime? ExpiresAt { get; set; }
        [JsonPropertyName("access_level")]
        public int AccessLevel { get; set; }
    }
}
