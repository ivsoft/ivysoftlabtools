﻿namespace IVySoft.Gitlab.Api.Models.Members
{
    public interface ISharedWithGroup
    {
        long GroupId { get; }
        string GroupName { get; }
        string GroupFullPath { get; }
        MemberAccessLevel AccessLevel { get; }
    }
}