﻿using System.Runtime.CompilerServices;
using IVySoft.Gitlab.Api.Models.Pipelines.Requests;

namespace IVySoft.Gitlab.Api.Models.Pipelines
{
    public interface IPipelines
    {
        IAsyncEnumerable<IPipeline> Query(PipelinesQuery query, CancellationToken cancellationToken);
        ValueTask<IPipeline?> SingleOrDefault(PipelineSingleQuery query, CancellationToken cancellationToken);
    }
}