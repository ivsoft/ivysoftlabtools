﻿using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.Pipelines
{
    internal class Pipeline : IPipeline
    {
        [JsonPropertyName("id")]
        public long Id { get; }
        [JsonPropertyName("iid")]
        public long IId { get; }
        [JsonPropertyName("status")]
        public string Status { get; }

        [JsonPropertyName("web_url")]
        public string WebUrl { get; }

        public Pipeline(long id, long iid, string status, string webUrl)
        {
            Id = id;
            IId = iid;
            Status = status;
            WebUrl = webUrl;
        }
    }
}