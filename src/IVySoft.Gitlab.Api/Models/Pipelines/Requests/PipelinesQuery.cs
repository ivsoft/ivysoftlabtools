﻿using IVySoft.Gitlab.Api.Models;

namespace IVySoft.Gitlab.Api.Models.Pipelines.Requests
{
    public class PipelinesQuery
    {
        public string IdOrPathWithNamespace { get; }

        public PipelinesQuery(string idOrPathWithNamespace)
        {
            IdOrPathWithNamespace = idOrPathWithNamespace;
        }

        public int PerPage { get; set; } = 100;
        public PipelineOrder? Order { get; set; }
        public SortOrder? SortOrder { get; set; }
        public string? Ref { get; set; }
        public string? Sha { get; set; }

        internal void Apply(Dictionary<string, string> param)
        {
            param.Add("per_page", PerPage.ToString());
            if (null != Order)
            {
                switch (Order)
                {
                    case PipelineOrder.Id:
                        param.Add("order_by", "id");
                        break;
                    case PipelineOrder.Status:
                        param.Add("order_by", "status");
                        break;
                    case PipelineOrder.Ref:
                        param.Add("order_by", "ref");
                        break;
                    case PipelineOrder.UpdatedAt:
                        param.Add("order_by", "updated_at");
                        break;
                    case PipelineOrder.UserId:
                        param.Add("order_by", "user_id");
                        break;
                }
            }
            if (null != SortOrder)
            {
                switch (SortOrder)
                {
                    case Models.SortOrder.Descending:
                        param.Add("sort", "desc");
                        break;
                    case Models.SortOrder.Ascending:
                        param.Add("sort", "asc");
                        break;
                }
            }
            if (null != Ref)
            {
                param.Add("ref", Ref);
            }
            if (null != Sha)
            {
                param.Add("sha", Sha);
            }
        }
    }
}