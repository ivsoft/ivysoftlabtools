﻿namespace IVySoft.Gitlab.Api.Models.Pipelines.Requests
{
    public class PipelineSingleQuery
    {
        public string IdOrPathWithNamespace { get; }
        public long PipelineId { get; }

        public PipelineSingleQuery(string idOrPathWithNamespace, long pipelineId)
        {
            IdOrPathWithNamespace = idOrPathWithNamespace;
            PipelineId = pipelineId;
        }
    }
}