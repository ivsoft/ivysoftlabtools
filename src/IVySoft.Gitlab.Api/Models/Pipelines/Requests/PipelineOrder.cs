﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IVySoft.Gitlab.Api.Models.Pipelines.Requests
{
    public enum PipelineOrder
    {
        Id,
        Status,
        Ref,
        UpdatedAt,
        UserId
    }
}
