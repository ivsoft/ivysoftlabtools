﻿using IVySoft.Gitlab.Api.Models.Pipelines.Requests;
using Microsoft.AspNetCore.WebUtilities;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text.Json;

namespace IVySoft.Gitlab.Api.Models.Pipelines
{
    internal class PipelinesProvider : IPipelines
    {
        private readonly IGitLabClient _client;

        public PipelinesProvider(IGitLabClient client)
        {
            _client = client;
        }

        public async IAsyncEnumerable<IPipeline> Query(PipelinesQuery query, [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            var url = $"projects/{WebUtility.UrlEncode(query.IdOrPathWithNamespace)}/pipelines";
            var param = new Dictionary<string, string>();
            query.Apply(param);
            var baseUrl = QueryHelpers.AddQueryString(url, param);

            for (var start_page = 1; ; ++start_page)
            {
                using var request = new HttpRequestMessage(HttpMethod.Get, $"{baseUrl}&page={start_page}");
                var response = await _client.Send(request, cancellationToken);
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Failed {response.Content.ReadAsStringAsync().Result} at get projects list");
                }
                int count = 0;
                await foreach (var pipeline in JsonSerializer.DeserializeAsyncEnumerable<Pipeline>(
                    await response.Content.ReadAsStreamAsync(cancellationToken),
                    ThisAssemblyJsonSerializerContext.Default.Options,
                    cancellationToken))
                {
                    if (null != pipeline)
                    {
                        yield return pipeline;
                        ++count;
                    }
                }
                if(0 == count)
                {
                    break;
                }
            }
        }

        public async ValueTask<IPipeline?> SingleOrDefault(PipelineSingleQuery query, CancellationToken cancellationToken)
        {
            var url = $"projects/{WebUtility.UrlEncode(query.IdOrPathWithNamespace)}/pipelines/{query.PipelineId}";
            using var request = new HttpRequestMessage(HttpMethod.Get, url);
            var response = await _client.Send(request, cancellationToken);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Failed {response.Content.ReadAsStringAsync().Result} at get projects list");
            }
            return await JsonSerializer.DeserializeAsync<Pipeline>(
                await response.Content.ReadAsStreamAsync(cancellationToken),
                ThisAssemblyJsonSerializerContext.Default.Options,
                cancellationToken);
        }
    }
}