﻿namespace IVySoft.Gitlab.Api.Models.Pipelines
{
    public interface IPipeline
    {
        long Id { get; }
        long IId { get; }
        string WebUrl { get; }
        string Status { get; }
    }
}