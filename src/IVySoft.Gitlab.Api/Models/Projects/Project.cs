﻿using IVySoft.Gitlab.Api.Models.Members;
using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.Projects
{
    internal class Project : IProject
    {
        [JsonPropertyName("id")]
        public long Id { get; }
        [JsonPropertyName("name_with_namespace")]
        public string NameWithNamespace { get; }
        [JsonPropertyName("name")]
        public string Name { get; }
        [JsonPropertyName("path_with_namespace")]
        public string PathWithNamespace { get; }
        [JsonPropertyName("web_url")]
        public string WebUrl { get; }
        [JsonPropertyName("shared_with_groups")]
        public SharedWithGroup[]? SharedWithGroups { get; }
        ISharedWithGroup[] IProject.SharedWithGroups => SharedWithGroups ?? Array.Empty<ISharedWithGroup>();

        public Project(
            long id,
            string nameWithNamespace,
            string name,
            string pathWithNamespace,
            string webUrl,
            SharedWithGroup[] sharedWithGroups)
        {
            Id = id;
            NameWithNamespace = nameWithNamespace;
            Name = name;
            PathWithNamespace = pathWithNamespace;
            WebUrl = webUrl;
            SharedWithGroups = sharedWithGroups;
        }
    }
}