﻿using System.Net;
using System.Runtime.CompilerServices;
using System.Text.Json;
using IVySoft.Gitlab.Api.Models.Members;
using IVySoft.Gitlab.Api.Models.Packages;
using IVySoft.Gitlab.Api.Models.Projects.Requests;

namespace IVySoft.Gitlab.Api.Models.Projects
{
    internal class ProjectsProvider : IProjects
    {
        private readonly IGitLabClient _client;

        public ProjectsProvider(IGitLabClient client)
        {
            _client = client;
        }

        public async ValueTask<IProject?> SingleOrDefault(ProjectSingleQuery query, CancellationToken cancellationToken)
        {
            var requestString = $"projects/{WebUtility.UrlEncode(query.IdOrPathWithNamespace)}";
            using var request = new HttpRequestMessage(HttpMethod.Get, requestString);
            var response = await _client.Send(request, cancellationToken);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Failed {response.Content.ReadAsStringAsync().Result} at get project");
            }
            return await JsonSerializer.DeserializeAsync<Project>(
                await response.Content.ReadAsStreamAsync(cancellationToken),
                ThisAssemblyJsonSerializerContext.Default.Project,
                cancellationToken);
        }
        public async ValueTask<IJobTokenScope> GetJobTokenScope(string idOrPathWithNamespace, CancellationToken cancellationToken)
        {
            var requestString = $"projects/{WebUtility.UrlEncode(idOrPathWithNamespace)}/job_token_scope";
            using var request = new HttpRequestMessage(HttpMethod.Get, requestString);
            var response = await _client.Send(request, cancellationToken);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Failed {response.Content.ReadAsStringAsync().Result} at get project {idOrPathWithNamespace} job_token_scope");
            }
            return await JsonSerializer.DeserializeAsync<JobTokenScope>(
                await response.Content.ReadAsStreamAsync(cancellationToken),
                ThisAssemblyJsonSerializerContext.Default.Options,
                cancellationToken)
                ?? throw new Exception($"Failed get project {idOrPathWithNamespace} job_token_scope");
        }

        public async IAsyncEnumerable<IProject> GetJobTokenScopeAllowList(string idOrPathWithNamespace, [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            for (var start_page = 1; ; ++start_page)
            {
                using var request = new HttpRequestMessage(HttpMethod.Get,
                    $"projects/{WebUtility.UrlEncode(idOrPathWithNamespace)}/job_token_scope/allowlist?page={start_page}");
                var response = await _client.Send(request, cancellationToken);
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Failed {await response.Content.ReadAsStringAsync()} at get allow list of job token scope");
                }
                int count = 0;
                await foreach (var project in JsonSerializer.DeserializeAsyncEnumerable<Project>(
                    await response.Content.ReadAsStreamAsync(cancellationToken),
                    ThisAssemblyJsonSerializerContext.Default.Options,
                    cancellationToken))
                {
                    if (null != project)
                    {
                        yield return project;
                        ++count;
                    }
                }
                if (0 == count)
                {
                    break;
                }
            }
        }

        public async IAsyncEnumerable<IMember> GetMembers(
            ProjectMembersQuery query,
            [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            var url = $"projects/{query.ProjectId}/members";
            var param = new QueryBuilder();
            query.Apply(param);

            for (var start_page = 1; ; ++start_page)
            {
                using var request = new HttpRequestMessage(HttpMethod.Get, param.Build(url, start_page));
                var response = await _client.Send(request, cancellationToken);
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Failed {await response.Content.ReadAsStringAsync()} at get group members");
                }
                int count = 0;
                await foreach (var member in JsonSerializer.DeserializeAsyncEnumerable<Member>(
                    await response.Content.ReadAsStreamAsync(cancellationToken),
                    ThisAssemblyJsonSerializerContext.Default.Options,
                    cancellationToken))
                {
                    if (null != member)
                    {
                        yield return member;
                        ++count;
                    }
                }
                if (0 == count)
                {
                    break;
                }
            }
        }
        public async IAsyncEnumerable<IMember> GetAllMembers(
            ProjectMembersQuery query,
            [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            var url = $"projects/{query.ProjectId}/members/all";
            var param = new QueryBuilder();
            query.Apply(param);

            for (var start_page = 1; ; ++start_page)
            {
                using var request = new HttpRequestMessage(HttpMethod.Get, param.Build(url, start_page));
                var response = await _client.Send(request, cancellationToken);
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Failed {await response.Content.ReadAsStringAsync()} at get group members");
                }
                int count = 0;
                await foreach (var member in JsonSerializer.DeserializeAsyncEnumerable<Member>(
                    await response.Content.ReadAsStreamAsync(cancellationToken),
                    ThisAssemblyJsonSerializerContext.Default.Options,
                    cancellationToken))
                {
                    if (null != member)
                    {
                        yield return member;
                        ++count;
                    }
                }
                if (0 == count)
                {
                    break;
                }
            }
        }
    }
}
