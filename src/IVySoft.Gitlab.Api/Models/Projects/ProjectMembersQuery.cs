﻿namespace IVySoft.Gitlab.Api.Models.Projects
{
    public class ProjectMembersQuery
    {
        public long ProjectId { get; }

        public ProjectMembersQuery(long projectId)
        {
            ProjectId = projectId;
        }
        public int PerPage { get; set; } = 100;
        public string OrderBy { get; set; } = string.Empty;
        public SortOrder Sort { get; set; } = SortOrder.Ascending;

        internal void Apply(QueryBuilder param)
        {
            param.Add("per_page", PerPage.ToString());
            if (!string.IsNullOrWhiteSpace(OrderBy))
            {
                param.Add("order_by", OrderBy);
                param.Add("sort", Sort == SortOrder.Ascending ? "asc" : "desc");
            }
        }
    }
}