﻿using IVySoft.Gitlab.Api.Models.Members;

namespace IVySoft.Gitlab.Api.Models.Projects
{
    public interface IProject
    {
        long Id { get; }
        string NameWithNamespace { get; }
        string PathWithNamespace { get; }
        string Name { get; }
        string WebUrl { get; }
        public ISharedWithGroup[] SharedWithGroups { get; }
    }
}