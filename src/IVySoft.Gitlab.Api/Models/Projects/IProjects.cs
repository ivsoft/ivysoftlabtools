﻿using IVySoft.Gitlab.Api.Models.Members;
using IVySoft.Gitlab.Api.Models.Projects.Requests;

namespace IVySoft.Gitlab.Api.Models.Projects
{
    public interface IProjects
    {
        ValueTask<IProject?> SingleOrDefault(ProjectSingleQuery query, CancellationToken cancellationToken);
        ValueTask<IJobTokenScope> GetJobTokenScope(string idOrPathWithNamespace, CancellationToken cancellationToken);
        IAsyncEnumerable<IProject> GetJobTokenScopeAllowList(string idOrPathWithNamespace, CancellationToken cancellationToken);
        IAsyncEnumerable<IMember> GetMembers(ProjectMembersQuery query, CancellationToken cancellationToken);
        IAsyncEnumerable<IMember> GetAllMembers(ProjectMembersQuery query, CancellationToken cancellationToken);
    }
}