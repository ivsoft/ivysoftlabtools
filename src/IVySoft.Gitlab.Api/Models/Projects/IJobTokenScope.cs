﻿namespace IVySoft.Gitlab.Api.Models.Projects
{
    public interface IJobTokenScope
    {
        bool InboundEnabled { get; }
    }
}