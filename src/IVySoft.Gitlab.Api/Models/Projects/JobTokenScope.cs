﻿using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.Projects
{
    internal class JobTokenScope : IJobTokenScope
    {
        [JsonPropertyName("inbound_enabled")]
        public bool InboundEnabled { get; set; }
    }
}