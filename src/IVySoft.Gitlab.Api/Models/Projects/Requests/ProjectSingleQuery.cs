﻿namespace IVySoft.Gitlab.Api.Models.Projects.Requests
{
    public class ProjectSingleQuery
    {
        /// <summary>
        /// The ID or path of the project.
        /// </summary>
        public string IdOrPathWithNamespace { get; }

        public ProjectSingleQuery(string idOrPathWithNamespace)
        {
            IdOrPathWithNamespace = idOrPathWithNamespace;
        }
    }
}