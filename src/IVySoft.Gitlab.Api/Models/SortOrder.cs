﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IVySoft.Gitlab.Api.Models
{
    public enum SortOrder
    {
        Descending,
        Ascending,
    }
}
