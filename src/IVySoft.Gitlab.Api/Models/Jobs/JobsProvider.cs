﻿using IVySoft.Gitlab.Api.Models.Jobs.Requests;
using IVySoft.Gitlab.Api.Models.MergeRequests;
using IVySoft.Gitlab.Api.Models.MergeRequests.Requests;
using Microsoft.AspNetCore.WebUtilities;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text.Json;

namespace IVySoft.Gitlab.Api.Models.Jobs
{
    internal class JobsProvider : IJobs
    {
        private readonly IGitLabClient _client;

        public JobsProvider(IGitLabClient client)
        {
            _client = client;
        }

        public async IAsyncEnumerable<IJob> Query(JobsQuery query, [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            foreach (var jobType in new string[]{ "jobs", "bridges"})
            {
                var url = $"projects/{WebUtility.UrlEncode(query.IdOrPathWithNamespace)}/pipelines/{query.PipelineId}/{jobType}";
                var param = new QueryBuilder();
                query.Apply(param);

                for (var start_page = 1; ; ++start_page)
                {
                    using var request = new HttpRequestMessage(HttpMethod.Get, param.Build(url, start_page));
                    var response = await _client.Send(request, cancellationToken);
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception($"Failed {response.Content.ReadAsStringAsync().Result} at get merge request list");
                    }
                    int count = 0;
                    await foreach (var job in JsonSerializer.DeserializeAsyncEnumerable<Job>(
                        await response.Content.ReadAsStreamAsync(cancellationToken),
                        ThisAssemblyJsonSerializerContext.Default.Options,
                        cancellationToken))
                    {
                        if (null != job)
                        {
                            yield return job;
                            ++count;
                        }
                        else
                        {
                            throw new NullReferenceException("Unable ot deserialize job info");
                        }
                    }
                    if (0 == count)
                    {
                        break;
                    }
                }
            }
        }

        public async ValueTask Run(long projectId, long jobId, CancellationToken cancellationToken)
        {
            using var request = new HttpRequestMessage(
                HttpMethod.Post,
                $"projects/{projectId}/jobs/{jobId}/play");
            var response = await _client.Send(request, cancellationToken);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Failed {response.Content.ReadAsStringAsync().Result} at start job {jobId} in project {projectId}");
            }
        }
    }
}