﻿using IVySoft.Gitlab.Api.Models.Pipelines.Requests;

namespace IVySoft.Gitlab.Api.Models.Jobs.Requests
{
    public class JobsQuery
    {
        public string IdOrPathWithNamespace { get; }
        public long PipelineId { get; }

        public JobsQuery(string idOrPathWithNamespace, long pipelineId)
        {
            IdOrPathWithNamespace = idOrPathWithNamespace;
            PipelineId = pipelineId;
        }

        public int PerPage { get; set; } = 100;
        public string[] Scope { get; set; } = Array.Empty<string>();

        internal void Apply(QueryBuilder param)
        {
            param.Add("per_page", PerPage.ToString());
            param.Add("scope", Scope);
        }
    }
}