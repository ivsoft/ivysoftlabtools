﻿using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.Jobs
{
    internal class Job : IJob
    {
        [JsonPropertyName("id")]
        public long Id { get; }
        [JsonPropertyName("name")]
        public string Name { get; }
        [JsonPropertyName("status")]
        public string Status { get; }

        [JsonPropertyName("web_url")]
        public string WebUrl { get; }

        public Job(long id, string name, string status, string webUrl)
        {
            Id = id;
            Name = name;
            Status = status;
            WebUrl = webUrl;
        }
    }
}
