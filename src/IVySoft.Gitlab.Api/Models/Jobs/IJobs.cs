﻿using IVySoft.Gitlab.Api.Models.Jobs.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IVySoft.Gitlab.Api.Models.Jobs
{
    public interface IJobs
    {
        IAsyncEnumerable<IJob> Query(JobsQuery query, CancellationToken cancellationToken);
        ValueTask Run(long projectId, long jobId, CancellationToken cancellationToken);
    }
}
