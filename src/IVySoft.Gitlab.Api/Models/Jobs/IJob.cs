﻿namespace IVySoft.Gitlab.Api.Models.Jobs
{
    public interface IJob
    {
        long Id { get; }
        string Name { get; }
        string Status { get; }
        string WebUrl { get; }
    }
}
