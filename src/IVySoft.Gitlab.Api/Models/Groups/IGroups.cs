﻿using IVySoft.Gitlab.Api.Models.Members;
using IVySoft.Gitlab.Api.Models.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IVySoft.Gitlab.Api.Models.Groups
{
    public interface IGroups
    {
        ValueTask<IGroup> Single(long groupId, CancellationToken cancellationToken);
        IAsyncEnumerable<IGroup> SubGroups(long groupId, CancellationToken cancellationToken);
        IAsyncEnumerable<IProject> SubProjects(long groupId, CancellationToken cancellationToken);
        IAsyncEnumerable<IMember> GetMembers(GroupMembersQuery query, CancellationToken cancellationToken);
        IAsyncEnumerable<IMember> GetAllMembers(GroupMembersQuery query, CancellationToken cancellationToken);
        IAsyncEnumerable<IGroupMember> GetGroupMembers(GroupMembersQuery query, CancellationToken cancellationToken);
    }
}
