﻿using IVySoft.Gitlab.Api.Models.Members;
using IVySoft.Gitlab.Api.Models.Packages;
using IVySoft.Gitlab.Api.Models.Projects;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace IVySoft.Gitlab.Api.Models.Groups
{
    internal class GroupsProvider : IGroups
    {
        private readonly IGitLabClient _client;

        public GroupsProvider(IGitLabClient client)
        {
            _client = client;
        }

        public async IAsyncEnumerable<IMember> GetMembers(
            GroupMembersQuery query,
            [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            var url = $"groups/{query.GroupId}/members";
            var param = new QueryBuilder();
            query.Apply(param);

            for (var start_page = 1; ; ++start_page)
            {
                using var request = new HttpRequestMessage(HttpMethod.Get, param.Build(url, start_page));
                var response = await _client.Send(request, cancellationToken);
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Failed {await response.Content.ReadAsStringAsync()} at get group members");
                }
                int count = 0;
                await foreach (var member in JsonSerializer.DeserializeAsyncEnumerable<Member>(
                    await response.Content.ReadAsStreamAsync(cancellationToken),
                    ThisAssemblyJsonSerializerContext.Default.Options,
                    cancellationToken))
                {
                    if (null != member)
                    {
                        yield return member;
                        ++count;
                    }
                }
                if (0 == count)
                {
                    break;
                }
            }
        }
        public async IAsyncEnumerable<IMember> GetAllMembers(
            GroupMembersQuery query,
            [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            var url = $"groups/{query.GroupId}/members/all";
            var param = new QueryBuilder();
            query.Apply(param);

            for (var start_page = 1; ; ++start_page)
            {
                using var request = new HttpRequestMessage(HttpMethod.Get, param.Build(url, start_page));
                var response = await _client.Send(request, cancellationToken);
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Failed {await response.Content.ReadAsStringAsync()} at get group members");
                }
                int count = 0;
                await foreach (var member in JsonSerializer.DeserializeAsyncEnumerable<Member>(
                    await response.Content.ReadAsStreamAsync(cancellationToken),
                    ThisAssemblyJsonSerializerContext.Default.Options,
                    cancellationToken))
                {
                    if (null != member)
                    {
                        yield return member;
                        ++count;
                    }
                }
                if (0 == count)
                {
                    break;
                }
            }
        }
        public async IAsyncEnumerable<IGroupMember> GetGroupMembers(
            GroupMembersQuery query,
            [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            var url = $"groups/{query.GroupId}/group_members";
            var param = new QueryBuilder();
            query.Apply(param);

            for (var start_page = 1; ; ++start_page)
            {
                using var request = new HttpRequestMessage(HttpMethod.Get, param.Build(url, start_page));
                var response = await _client.Send(request, cancellationToken);
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Failed {await response.Content.ReadAsStringAsync()} at get group members");
                }
                var body = await response.Content.ReadAsStreamAsync(cancellationToken);
                int count = 0;
                await foreach (var member in JsonSerializer.DeserializeAsyncEnumerable<GroupMember>(
                    await response.Content.ReadAsStreamAsync(cancellationToken),
                    ThisAssemblyJsonSerializerContext.Default.Options,
                    cancellationToken))
                {
                    if (null != member)
                    {
                        yield return member;
                        ++count;
                    }
                }
                if (0 == count)
                {
                    break;
                }
            }
        }

        public async IAsyncEnumerable<IGroup> SubGroups(
            long groupId,
            [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            var baseUrl = $"groups/{groupId}/subgroups";

            for (var start_page = 1; ; ++start_page)
            {
                using var request = new HttpRequestMessage(HttpMethod.Get, $"{baseUrl}?page={start_page}");
                var response = await _client.Send(request, cancellationToken);
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Failed {await response.Content.ReadAsStringAsync()} at get groups list");
                }
                int count = 0;
                await foreach (var group in JsonSerializer.DeserializeAsyncEnumerable<Group>(
                    await response.Content.ReadAsStreamAsync(cancellationToken),
                    ThisAssemblyJsonSerializerContext.Default.Options,
                    cancellationToken))
                {
                    if (null != group)
                    {
                        yield return group;
                        ++count;
                    }
                }
                if (0 == count)
                {
                    break;
                }
            }
        }
        public async IAsyncEnumerable<IProject> SubProjects(
            long groupId,
            [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            var baseUrl = $"groups/{groupId}/projects";

            for (var start_page = 1; ; ++start_page)
            {
                using var request = new HttpRequestMessage(HttpMethod.Get, $"{baseUrl}?page={start_page}");
                var response = await _client.Send(request, cancellationToken);
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Failed {await response.Content.ReadAsStringAsync()} at get groups list");
                }
                int count = 0;
                await foreach (var project in JsonSerializer.DeserializeAsyncEnumerable<Project>(
                    await response.Content.ReadAsStreamAsync(cancellationToken),
                    ThisAssemblyJsonSerializerContext.Default.Options,
                    cancellationToken))
                {
                    if (null != project)
                    {
                        yield return project;
                        ++count;
                    }
                }
                if (0 == count)
                {
                    break;
                }
            }
        }

        public async ValueTask<IGroup> Single(long groupId, CancellationToken cancellationToken)
        {
            var requestString = $"groups/{groupId}";
            using var request = new HttpRequestMessage(HttpMethod.Get, requestString);
            var response = await _client.Send(request, cancellationToken);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Failed {response.Content.ReadAsStringAsync().Result} at get group");
            }
            var body = await response.Content.ReadAsStringAsync(cancellationToken);
            return JsonSerializer.Deserialize<Group>(
                body,
                ThisAssemblyJsonSerializerContext.Default.Options)
            ?? throw new NullReferenceException("Unable to deserialize group from '{body}'");
        }
    }
}
