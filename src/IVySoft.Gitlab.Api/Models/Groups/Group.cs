﻿using IVySoft.Gitlab.Api.Models.Members;
using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.Groups
{
    internal class Group : IGroup
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; } = null!;
        [JsonPropertyName("path")]
        public string Path { get; set; } = null!;
        [JsonPropertyName("visibility")]
        public string Visibility { get; set; } = null!;
        [JsonPropertyName("web_url")]
        public string WebUrl { get; set; } = null!;
        [JsonPropertyName("shared_with_groups")]
        public SharedWithGroup[]? SharedWithGroups { get; set; }
        ISharedWithGroup[] IGroup.SharedWithGroups => SharedWithGroups ?? Array.Empty<ISharedWithGroup>();
    }
}