﻿
namespace IVySoft.Gitlab.Api.Models.Groups
{
    public class GroupMembersQuery
    {
        public long GroupId { get; }

        public GroupMembersQuery(long groupId)
        {
            GroupId = groupId;
        }
        public int PerPage { get; set; } = 100;
        public string OrderBy { get; set; } = string.Empty;
        public SortOrder Sort { get; set; } = SortOrder.Ascending;

        internal void Apply(QueryBuilder param)
        {
            param.Add("per_page", PerPage.ToString());
            if (!string.IsNullOrWhiteSpace(OrderBy))
            {
                param.Add("order_by", OrderBy);
                param.Add("sort", Sort == SortOrder.Ascending ? "asc" : "desc");
            }
        }
    }
}