﻿using System.Text.Json.Serialization;
using IVySoft.Gitlab.Api.Models.Members;

namespace IVySoft.Gitlab.Api.Models.Groups
{
    public interface IGroup
    {
        public int Id { get; }
        public string Name { get; } 
        public string Path { get; }
        public string Visibility { get; }
        public string WebUrl { get; }
        public ISharedWithGroup[] SharedWithGroups { get; }
    }
}
