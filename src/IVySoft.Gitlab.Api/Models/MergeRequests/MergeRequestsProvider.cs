﻿using IVySoft.Gitlab.Api.Models.MergeRequests.Requests;
using Microsoft.AspNetCore.WebUtilities;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text.Json;

namespace IVySoft.Gitlab.Api.Models.MergeRequests
{
    internal class MergeRequestsProvider : IMergeRequests
    {
        private readonly IGitLabClient _client;

        public MergeRequestsProvider(IGitLabClient client)
        {
            _client = client;
        }

        public async IAsyncEnumerable<IMergeRequest> Query(MergeRequestsQuery query, [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            var url = $"projects/{WebUtility.UrlEncode(query.IdOrPathWithNamespace)}/merge_requests";
            var param = new Dictionary<string, string>();
            query.Apply(param);
            var baseUrl = QueryHelpers.AddQueryString(url, param);

            for (var start_page = 1; ; ++start_page)
            {
                using var request = new HttpRequestMessage(HttpMethod.Get, $"{baseUrl}&page={start_page}");
                var response = await _client.Send(request, cancellationToken);
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Failed {response.Content.ReadAsStringAsync().Result} at get merge request list");
                }
                int count = 0;
                await foreach (var mergeRequest in JsonSerializer.DeserializeAsyncEnumerable<MergeRequest>(
                    await response.Content.ReadAsStreamAsync(cancellationToken),
                    ThisAssemblyJsonSerializerContext.Default.Options,
                    cancellationToken))
                {
                    if (null != mergeRequest)
                    {
                        yield return mergeRequest;
                        ++count;
                    }
                }
                if (0 == count)
                {
                    break;
                }
            }
        }
    }
}