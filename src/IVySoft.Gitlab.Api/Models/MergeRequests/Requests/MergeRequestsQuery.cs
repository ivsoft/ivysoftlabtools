﻿using IVySoft.Gitlab.Api.Models.Pipelines.Requests;

namespace IVySoft.Gitlab.Api.Models.MergeRequests.Requests
{
    public class MergeRequestsQuery
    {
        public string IdOrPathWithNamespace { get; }

        public MergeRequestsQuery(string idOrPathWithNamespace)
        {
            IdOrPathWithNamespace = idOrPathWithNamespace;
        }

        public int PerPage { get; set; } = 100;
        public MergeRequestOrder? Order { get; set; }
        public SortOrder? SortOrder { get; set; }
        public string? SourceBranch { get; set; }
        public string? TargetBranch { get; set; }
        public string? State { get; set; }

        internal void Apply(Dictionary<string, string> param)
        {
            param.Add("per_page", PerPage.ToString());
            if (null != Order)
            {
                switch (Order)
                {
                    case MergeRequestOrder.CreatedAt:
                        param.Add("order_by", "created_at");
                        break;
                    case MergeRequestOrder.UpdatedAt:
                        param.Add("order_by", "updated_at");
                        break;
                    case MergeRequestOrder.Title:
                        param.Add("order_by", "title");
                        break;
                }
            }
            if (null != SortOrder)
            {
                switch (SortOrder)
                {
                    case Models.SortOrder.Descending:
                        param.Add("sort", "desc");
                        break;
                    case Models.SortOrder.Ascending:
                        param.Add("sort", "asc");
                        break;
                }
            }
            if (!string.IsNullOrWhiteSpace(SourceBranch))
            {
                param.Add("source_branch", SourceBranch);
            }
            if (!string.IsNullOrWhiteSpace(TargetBranch))
            {
                param.Add("target_branch", TargetBranch);
            }
            if (!string.IsNullOrWhiteSpace(State))
            {
                param.Add("state", State);
            }
        }
    }
}