﻿namespace IVySoft.Gitlab.Api.Models.MergeRequests.Requests
{
    public enum MergeRequestOrder
    {
        CreatedAt,
        Title,
        UpdatedAt
    }
}