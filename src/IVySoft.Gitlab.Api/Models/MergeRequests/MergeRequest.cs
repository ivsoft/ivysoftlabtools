﻿using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.MergeRequests
{
    internal class MergeRequest : IMergeRequest
    {
        [JsonPropertyName("id")]
        public long Id { get; }
        [JsonPropertyName("iid")]
        public long IId { get; }
        [JsonPropertyName("state")]
        public string State { get; }
        public MergeRequest(long id, long iId, string state)
        {
            Id = id;
            IId = iId;
            State = state;
        }
    }
}