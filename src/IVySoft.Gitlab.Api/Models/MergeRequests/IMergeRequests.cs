﻿using IVySoft.Gitlab.Api.Models.MergeRequests.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IVySoft.Gitlab.Api.Models.MergeRequests
{
    public interface IMergeRequests
    {
        IAsyncEnumerable<IMergeRequest> Query(MergeRequestsQuery query, CancellationToken cancellationToken);
    }
}
