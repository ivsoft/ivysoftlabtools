﻿namespace IVySoft.Gitlab.Api.Models.MergeRequests
{
    public interface IMergeRequest
    {
        long Id { get; }
        long IId { get; }
        string State { get; }
    }
}