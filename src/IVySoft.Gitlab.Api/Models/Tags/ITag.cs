﻿namespace IVySoft.Gitlab.Api.Models.Tags
{
    public interface ITag
    {
        string Name { get; }
        string? Message { get; }
        string Target { get; }
        ICommit Commit { get; }
    }
}