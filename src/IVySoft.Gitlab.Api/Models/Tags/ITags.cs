﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IVySoft.Gitlab.Api.Models.Tags
{
    public interface ITags
    {
        ValueTask<ITag?> SingleOrDefault(string projectIdOrPathWithNamespace, string tag, CancellationToken cancellationToken);
    }
}
