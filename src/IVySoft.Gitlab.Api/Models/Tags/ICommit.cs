﻿namespace IVySoft.Gitlab.Api.Models.Tags
{
    public interface ICommit
    {
        string Id { get; }
        string ShortId { get; }
        string Title { get; }
        string[] ParentIds { get; }
    }
}