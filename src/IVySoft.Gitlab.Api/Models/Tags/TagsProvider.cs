﻿
using System.Net;
using System.Text.Json;

namespace IVySoft.Gitlab.Api.Models.Tags
{
    internal sealed class TagsProvider : ITags
    {
        private readonly IGitLabClient _client;

        public TagsProvider(IGitLabClient client)
        {
            _client = client;
        }

        public async ValueTask<ITag?> SingleOrDefault(string projectIdOrPathWithNamespace, string tag, CancellationToken cancellationToken)
        {
            var url = $"projects/{WebUtility.UrlEncode(projectIdOrPathWithNamespace)}/repository/tags/{WebUtility.UrlEncode(tag)}";
            using var request = new HttpRequestMessage(HttpMethod.Get, url);
            var response = await _client.Send(request, cancellationToken);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Failed {response.Content.ReadAsStringAsync().Result} at get project '{projectIdOrPathWithNamespace}' tag '{tag}' info");
            }
            return await JsonSerializer.DeserializeAsync<Tag>(
                await response.Content.ReadAsStreamAsync(cancellationToken),
                ThisAssemblyJsonSerializerContext.Default.Options,
                cancellationToken);
        }
    }
}
