﻿using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.Tags
{
    internal class Tag : ITag
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = null!;
        [JsonPropertyName("message")]
        public string? Message { get; set; }
        [JsonPropertyName("target")]
        public string Target { get; set; } = null!;
        [JsonPropertyName("commit")]
        public Commit Commit { get; set; } = null!;

        ICommit ITag.Commit => Commit;
    }
}