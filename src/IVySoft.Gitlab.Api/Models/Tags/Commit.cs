﻿using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.Tags
{
    internal class Commit : ICommit
    {
        [JsonPropertyName("id")]
        public string Id { get; set; } = null!;
        [JsonPropertyName("short_id")]
        public string ShortId { get; set; } = null!;
        [JsonPropertyName("title")]
        public string Title { get; set; } = null!;
        [JsonPropertyName("parent_ids")]
        public string[] ParentIds { get; set; } = null!;
    }
}