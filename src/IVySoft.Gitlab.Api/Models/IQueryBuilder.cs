﻿using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IVySoft.Gitlab.Api.Models
{
    public class QueryBuilder
    {
        private readonly List<(string, string)> _query = new List<(string, string)> ();
        public void Add(string key, string value)
        {
            _query.Add((key, value));
        }
        public void Add(string key, string[] value)
        {
            if (value.Length == 1)
            {
                _query.Add((key, value[0]));
            }
            else
            {
                key += "[]";
                foreach (var item in value)
                {
                    _query.Add((key, item));
                }
            }
        }
        internal string Build(string url, int page)
        {
            var sb = new StringBuilder(url);
            bool isFirst = true;
            foreach(var item in _query)
            {
                if (isFirst)
                {
                    sb.Append('?');
                    isFirst = false;
                }
                else
                {
                    sb.Append('&');
                }
                sb.Append(item.Item1);
                sb.Append('=');
                sb.Append(WebUtility.UrlEncode(item.Item2));
            }
            sb.Append(isFirst ? '?' : '&');
            sb.Append("page=");
            sb.Append(page);
            return sb.ToString();
        }
    }
}
