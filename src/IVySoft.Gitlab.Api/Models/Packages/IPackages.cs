﻿using IVySoft.Gitlab.Api.Models.Packages.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace IVySoft.Gitlab.Api.Models.Packages
{
    public interface IPackages
    {
        IAsyncEnumerable<IPackage> Query(PackagesQuery query, CancellationToken cancellationToken);
        IAsyncEnumerable<IPackageDependencyGroup> GetDependencies(
            string projectIdOrPathWithNamespace,
            string packageName,
            string packageVersion,
            CancellationToken cancellationToken);
    }
}

