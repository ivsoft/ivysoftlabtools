﻿using IVySoft.Gitlab.Api.Models.Jobs;
using IVySoft.Gitlab.Api.Models.Packages.Requests;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading;

namespace IVySoft.Gitlab.Api.Models.Packages
{
    internal class PackagesProvider : IPackages
    {
        private readonly IGitLabClient _client;

        public PackagesProvider(IGitLabClient client)
        {
            _client = client;
        }

        public async IAsyncEnumerable<IPackage> Query(PackagesQuery query, [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            var url = $"groups/{query.GroupId}/packages";
            var param = new QueryBuilder();
            query.Apply(param);

            for (var start_page = 1; ; ++start_page)
            {
                using var request = new HttpRequestMessage(HttpMethod.Get, param.Build(url, start_page));
                var response = await _client.Send(request, cancellationToken);
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Failed {await response.Content.ReadAsStringAsync()} at get package list");
                }
                int count = 0;
                await foreach (var package in JsonSerializer.DeserializeAsyncEnumerable<Package>(
                    await response.Content.ReadAsStreamAsync(cancellationToken),
                    ThisAssemblyJsonSerializerContext.Default.Options,
                    cancellationToken))
                {
                    if (null != package)
                    {
                        yield return package;
                        ++count;
                    }
                }
                if (0 == count)
                {
                    break;
                }
            }
        }
        public async IAsyncEnumerable<IPackageDependencyGroup> GetDependencies(
            string projectIdOrPathWithNamespace,
            string packageName,
            string packageVersion,
            [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            var url = $"projects/{WebUtility.UrlEncode(projectIdOrPathWithNamespace)}/packages/nuget/metadata/{WebUtility.UrlEncode(packageName)}/{WebUtility.UrlEncode(packageVersion)}";
            using var request = new HttpRequestMessage(HttpMethod.Get, url);
            var response = await _client.Send(request, cancellationToken);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Failed {await response.Content.ReadAsStringAsync()} at get package {packageName} {packageVersion} in project {projectIdOrPathWithNamespace}");
            }
            var package = await JsonSerializer.DeserializeAsync<PackageInfo>(
                    await response.Content.ReadAsStreamAsync(cancellationToken),
                    ThisAssemblyJsonSerializerContext.Default.Options,
                    cancellationToken);
            foreach(var  dependencyGroup in package!.CatalogEntry.DependencyGroups)
            {
                yield return dependencyGroup;
            }
        }
    }
}

