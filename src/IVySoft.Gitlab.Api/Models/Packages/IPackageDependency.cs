﻿namespace IVySoft.Gitlab.Api.Models.Packages
{
    public interface IPackageDependency
    {
        string PackageName { get; }
        string PackageRange { get; }
        string DependencyType { get; }
    }
}