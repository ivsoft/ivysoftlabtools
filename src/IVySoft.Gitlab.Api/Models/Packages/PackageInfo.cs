﻿using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.Packages
{
    internal class PackageInfo
    {
        [JsonPropertyName("id")]
        public string Id { get; set; } = null!;
        [JsonPropertyName("packageContent")]
        public string PackageContent { get; set; } = null!;
        [JsonPropertyName("catalogEntry")]
        public CatalogEntry CatalogEntry { get; set; } = null!;
    }
}