﻿namespace IVySoft.Gitlab.Api.Models.Packages
{
    public interface IPipelineInfo
    {
        long Id { get; set; }
        long IId { get; set; }
        long ProjectId { get; set; }
        string Sha { get; set; }
        string Ref { get; set; }
        string Status { get; set; }
    }
}