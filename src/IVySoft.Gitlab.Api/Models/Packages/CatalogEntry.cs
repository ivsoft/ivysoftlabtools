﻿using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.Packages
{
    internal class CatalogEntry
    {
        [JsonPropertyName("dependencyGroups")]
        public DependencyGroup[] DependencyGroups { get; set; } = null!;
    }
}