﻿using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.Packages
{
    internal class DependencyGroup : IPackageDependencyGroup
    {
        [JsonPropertyName("id")]
        public string Id { get; set; } = null!;
        [JsonPropertyName("type")]
        public string Type { get; set; } = null!;
        [JsonPropertyName("targetFramework")]
        public string TargetFramework { get; set; } = null!;
        [JsonPropertyName("dependencies")]
        public PackageDependency[] Dependencies { get; set; } = null!;

        IPackageDependency[] IPackageDependencyGroup.Dependencies => Dependencies;
    }
}