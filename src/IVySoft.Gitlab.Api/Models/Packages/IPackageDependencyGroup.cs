﻿namespace IVySoft.Gitlab.Api.Models.Packages
{
    public interface IPackageDependencyGroup
    {
        IPackageDependency[] Dependencies { get; }
        string TargetFramework { get; }
    }
}