﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IVySoft.Gitlab.Api.Models.Packages.Requests
{
    public class PackagesQuery
    {
        public int GroupId { get; }

        public PackagesQuery(int groupId)
        {
            GroupId = groupId;
        }

        public int PerPage { get; set; } = 100;
        public string PackageType { get; set; } = string.Empty;
        public string PackageName { get; set; } = string.Empty;
        public string PackageVersion { get; set; } = string.Empty;
        public string OrderBy { get; set; } = string.Empty;
        public SortOrder Sort { get; set; } = SortOrder.Ascending;

        internal void Apply(QueryBuilder param)
        {
            param.Add("per_page", PerPage.ToString());
            if (!string.IsNullOrWhiteSpace(PackageType))
            {
                param.Add("package_type", PackageType);
            }
            if (!string.IsNullOrWhiteSpace(PackageName))
            {
                param.Add("package_name", PackageName);
            }
            if (!string.IsNullOrWhiteSpace(PackageVersion))
            {
                param.Add("package_version", PackageVersion);
            }
            if (!string.IsNullOrWhiteSpace(OrderBy))
            {
                param.Add("order_by", OrderBy);
                param.Add("sort", Sort == SortOrder.Ascending ? "asc" : "desc");
            }
        }
    }
}
