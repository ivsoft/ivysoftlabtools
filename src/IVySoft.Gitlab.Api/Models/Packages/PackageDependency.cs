﻿using System.Text.Json.Serialization;

namespace IVySoft.Gitlab.Api.Models.Packages
{
    internal class PackageDependency : IPackageDependency
    {
        [JsonPropertyName("@id")]
        public string Id { get; set; } = null!;
        [JsonPropertyName("type")]
        public string DependencyType { get; set; } = null!;
        [JsonPropertyName("id")]
        public string PackageName { get; set; } = null!;
        [JsonPropertyName("range")]
        public string PackageRange { get; set; } = null!;
    }
}