﻿using System.Text.Json.Serialization;
using static IVySoft.Gitlab.Api.Models.Packages.Package;

namespace IVySoft.Gitlab.Api.Models.Packages
{
    internal class Package : IPackage
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; } = null!;
        [JsonPropertyName("version")]
        public string Version { get; set; } = null!;
        [JsonPropertyName("package_type")]
        public string PackageType { get; set; } = null!;
        [JsonPropertyName("project_id")]
        public long ProjectId { get; set; }

        [JsonPropertyName("pipeline")]
        internal PipelineInfo Pipeline { get; set; } = null!;

        IPipelineInfo IPackage.Pipeline => Pipeline;

        internal class PipelineInfo : IPipelineInfo
        {
            [JsonPropertyName("id")]
            public long Id { get; set; }
            [JsonPropertyName("iid")]
            public long IId { get; set; }
            [JsonPropertyName("project_id")]
            public long ProjectId { get; set; }
            [JsonPropertyName("sha")]
            public string Sha { get; set; } = null!;
            [JsonPropertyName("ref")]
            public string Ref { get; set; } = null!;
            [JsonPropertyName("status")]
            public string Status { get; set; } = null!;
        }
    }
}

