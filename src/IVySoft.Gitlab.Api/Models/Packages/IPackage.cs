﻿using static IVySoft.Gitlab.Api.Models.Packages.Package;

namespace IVySoft.Gitlab.Api.Models.Packages
{
    public interface IPackage
    {
        long Id { get; }
        string Name { get; }
        string Version { get; }
        IPipelineInfo Pipeline { get; }
        string PackageType { get; set; }
        long ProjectId { get; set; }
    }
}