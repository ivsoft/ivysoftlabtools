﻿using IVySoft.Gitlab.Api.Models.Groups;
using IVySoft.Gitlab.Api.Models.Jobs;
using IVySoft.Gitlab.Api.Models.Members;
using IVySoft.Gitlab.Api.Models.MergeRequests;
using IVySoft.Gitlab.Api.Models.Packages;
using IVySoft.Gitlab.Api.Models.Pipelines;
using IVySoft.Gitlab.Api.Models.Projects;
using IVySoft.Gitlab.Api.Models.Tags;
using System.Text.Json.Serialization;

[JsonSerializable(typeof(Project))]
[JsonSerializable(typeof(Pipeline))]
[JsonSerializable(typeof(MergeRequest))]
[JsonSerializable(typeof(Job))]
[JsonSerializable(typeof(Package))]
[JsonSerializable(typeof(Package.PipelineInfo))]
[JsonSerializable(typeof(PackageInfo))]
[JsonSerializable(typeof(CatalogEntry))]
[JsonSerializable(typeof(DependencyGroup))]
[JsonSerializable(typeof(PackageDependency))]
[JsonSerializable(typeof(JobTokenScope))]
[JsonSerializable(typeof(Group))]
[JsonSerializable(typeof(Member))]
[JsonSerializable(typeof(SharedWithGroup))]
[JsonSerializable(typeof(Tag))]
[JsonSerializable(typeof(Commit))]
internal partial class ThisAssemblyJsonSerializerContext : JsonSerializerContext
{

}