﻿using System.Linq.Expressions;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json.Serialization;
using IVySoft.Gitlab.Api.Models.Groups;
using IVySoft.Gitlab.Api.Models.Jobs;
using IVySoft.Gitlab.Api.Models.MergeRequests;
using IVySoft.Gitlab.Api.Models.Packages;
using IVySoft.Gitlab.Api.Models.Pipelines;
using IVySoft.Gitlab.Api.Models.Projects;
using IVySoft.Gitlab.Api.Models.Tags;

namespace IVySoft.Gitlab.Api
{
    public class GitLabClient : IGitLabClient, IDisposable
    {
        private readonly string _serverUrl;
        private readonly string _token;
        private readonly HttpClient _client;
        private readonly object _user;

        public GitLabClient(string serverUrl, string userName, string token)
        {
            _serverUrl = serverUrl;
            _user = userName;
            _token = token;

            _client = new HttpClient
            {
                BaseAddress = new Uri(new Uri(_serverUrl), "api/v4/")
            };

            Projects = new ProjectsProvider(this);
            Pipelines = new PipelinesProvider(this);
            MergeRequests = new MergeRequestsProvider(this);
            Jobs = new JobsProvider(this);
            Packages = new PackagesProvider(this);
            Groups = new GroupsProvider(this);
            Tags = new TagsProvider(this);
        }

        public IProjects Projects { get; }
        public IPipelines Pipelines { get; }
        public IMergeRequests MergeRequests { get; }
        public ITags Tags { get; }
        public IJobs Jobs { get; }
        public IPackages Packages { get; }
        public IGroups Groups { get; }

        public void Dispose()
        {
            _client.Dispose();
        }

        public async ValueTask<HttpResponseMessage> Send(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var byteArray = Encoding.ASCII.GetBytes($"{_user}:{_token}");
            var header = new AuthenticationHeaderValue(
                       "Basic", Convert.ToBase64String(byteArray));
            //_client.DefaultRequestHeaders.Authorization = header;

            request.Headers.Add("private-token", _token);
            request.Headers.Authorization = header;

            return await _client.SendAsync(request, cancellationToken);
        }
    }
}
