﻿using IVySoft.Gitlab.Api.Models.Groups;
using IVySoft.Gitlab.Api.Models.Jobs;
using IVySoft.Gitlab.Api.Models.MergeRequests;
using IVySoft.Gitlab.Api.Models.Pipelines;
using IVySoft.Gitlab.Api.Models.Projects;

namespace IVySoft.Gitlab.Api
{
    public interface IGitLabClient
    {
        IGroups Groups { get; }
        IProjects Projects { get; }
        IPipelines Pipelines { get; }
        IMergeRequests MergeRequests { get; }
        IJobs Jobs { get; }

        ValueTask<HttpResponseMessage> Send(HttpRequestMessage request, CancellationToken cancellationToken);
    }
}